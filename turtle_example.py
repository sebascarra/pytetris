import turtle

direction = 0

def move_snake():

    global direction

    pen.clearstamps()
    new_head = snake[-1].copy()
        
    direction = 0

    if direction % 4 == 0:
        new_head[0] += 20
    elif direction % 4 == 1:
        new_head[1] += 20
    elif direction % 4 == 2:
        new_head[0] -= 20
    else:
        new_head[1] -= 20

    direction += 1
    if direction == 4:
        direction = 0

    snake.append(new_head)
    snake.pop(0)

    print

    for segment in snake:
        pen.goto(segment[0], segment[1])
        pen.stamp()

    screen.update()
    turtle.ontimer(move_snake, 200)


snake = [[0, 0], [20, 0], [40, 0]]

screen = turtle.Screen()
screen.tracer(0)
pen = turtle.Turtle("square")
pen.penup()

#Must be a recursive function
move_snake()

#Must be at the end of the code for the interpreter to render the window properly
turtle.done()