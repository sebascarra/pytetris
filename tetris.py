from pprint import pprint as pp
from pieces import pieces, colors
import random
import turtle
from threading import Lock

turtle_size = 22
class Grid:

    def __init__(self, x, y):
        
        self._files = [[0] * x for _ in range(y)]
        self._y = y #Number of rows
        self._x = x #Number of columns

    def draw(self):

        global turtle_size
        global pen

        for y, ydraw in enumerate(range(-self._y // 2, self._y // 2)):
            for x, xdraw in enumerate(range(-self._x // 2, self._x // 2)):

                pen.goto(xdraw*turtle_size, ydraw*turtle_size)
                pen.color(colors[self._files[y][x]])
                pen.stamp()


    def tetris_check(self):

        with game.lock:

            score_modifier = 0
            lines_cleared = 0
            to_delete = []

            for idx, row in enumerate(self._files):
                if 0 not in row:
                    to_delete.append(idx)
                    lines_cleared += 1
                    score_modifier += 2

            for idx in to_delete:
                del self._files[idx]
                self._files.append([0] * self._x)

            if lines_cleared:
                game._lines_cleared += lines_cleared
                game._score += 100 * lines_cleared * score_modifier
                print ("Score:", game._score, " - Lines:", game._lines_cleared, end="\r")

        
class Piece:

    def __init__(self):

        self._shape = random.choice(list(pieces.keys()))
        self._y_piece = len(pieces[self._shape][0]) # Number of rows
        self._x_piece = len(pieces[self._shape][0][0]) # Number of columns - assumes piece has rectangular shape
        self.y_origin = grid._y - 1
        self.x_origin = (grid._x - self._x_piece) // 2
        
        self._color = 0
        self._state = random.choice(range(len(pieces[self._shape])))
        self.x_occupied = []
        self.y_occupied = []
        self.moving = False


    def spawn(self):

        with game.lock:

            x_occupied = []
            y_occupied = []

            for y, ydraw in zip(reversed(range(self._y_piece)), range(self.y_origin + 1 - self._y_piece, self.y_origin + 1)):
                for x, xdraw in enumerate(range(self.x_origin, self.x_origin + self._x_piece)):
                    
                    if pieces[self._shape][self._state][y][x] != 0:
                        if xdraw >= 0 and xdraw < grid._x and ydraw >= 0 and ydraw < grid._y and grid._files[ydraw][xdraw] == 0:
                            x_occupied.append(xdraw)
                            y_occupied.append(ydraw)
                            self._color = pieces[self._shape][self._state][y][x]
                    
                        else:
                            return False
        
            self.x_occupied = x_occupied.copy()
            self.y_occupied = y_occupied.copy()
            # print("Successful spawn")
            return True

            # spawn_coords = [[x, y] for x, y in zip(self.x_occupied, self.y_occupied)]
            # print(spawn_coords)


    def draw(self):

        global turtle_size
        global pen
        
        # pen.clearstamps()

        for y, x in zip(self.y_occupied, self.x_occupied):

            # print("x ", str(x), "y ", str(y))

            pen.goto(x*turtle_size - grid._x*turtle_size // 2, y*turtle_size - grid._y*turtle_size // 2)
            pen.color(colors[self._color])
            pen.stamp()


    def engrave(self):

        with game.lock:

                for y, x in zip(self.y_occupied, self.x_occupied):
                    grid._files[y][x] = self._color


    def move_down(self):

        with game.lock:

            # coords = [[x, y] for x, y in zip(self.x_occupied, self.y_occupied)]
            # print(coords)

            for x, y in zip(self.x_occupied, self.y_occupied):
                if y - 1 < 0 or grid._files[y-1][x] != 0:
                    return False

            self.y_occupied = [y - 1 for y in self.y_occupied]
            self.y_origin -= 1
            return True


    def move_side(self, side):

        with game.lock:

            if side == "left":
                val = -1
            else: # "right"
                val = 1
            for x, y in zip(self.x_occupied, self.y_occupied):
                if x+val < 0 or x+val >= grid._x or grid._files[y][x+val] != 0:
                    return False

            self.x_occupied = [x + val for x in self.x_occupied]
            self.x_origin += val
            return True

    def rotate(self):

        current_state = self._state
        self._state = (self._state + 1) % len(pieces[self._shape])
        if self.spawn():
            return True
        else:
            self._state = current_state
            return False


def move_right():

    game.piece.move_side("right")


def move_left():

    game.piece.move_side("left")


def rotate():

    game.piece.rotate()


def accelerate():

    game.piece.move_down()
    

class Game:

    def __init__(self):
        
        self._score = 0
        self._lines_cleared = 0
        self.piece = Piece()
        self._frame_count = 0
        print ("Score: 0 - Lines: 0", end="\r")
        self.lock = Lock()


    def run(self):

        if self.piece.moving == False:
            if self.piece.spawn():
                self.piece.moving = True
            else:
                active_game = False # You lose!
                print("\nYou lost!")
                return
            
        else:

            if self._frame_count == 4:
                self._frame_count = 0

                if self.piece.move_down() == False:
                    self.piece.moving = False
                    self.piece.engrave()
                    self.piece = Piece()

        self._frame_count += 1
        pen.clearstamps()
        grid.tetris_check()
        grid.draw()
        self.piece.draw()
        screen.update()

        screen.ontimer(self.run, 0)
        
if __name__ == "__main__":

    screen = turtle.Screen()
    screen.tracer(0)
    screen.listen()

    x = 10
    y = 20
    screen.setup(width=x*turtle_size+100, height=y*turtle_size+100)
    pen = turtle.Turtle("square")
    pen.penup()

    grid = Grid(x, y)

    game = Game()
    
    screen.onkeypress(move_right, "Right")
    screen.onkeypress(move_left, "Left")
    screen.onkeypress(rotate, "Up")
    screen.onkeypress(accelerate, "Down")

    active_game = True

    game.run()

    turtle.done()


